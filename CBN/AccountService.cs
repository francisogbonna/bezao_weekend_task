﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Text.RegularExpressions;

namespace WeekendTask_16_07_2021.CBN
{
    public delegate void WhistleBlowerEventHandler(object sender, EventArgs args);

    public class UserEventArgs : EventArgs
    {
        public UserAccount _user { get; set; }
    }
    
    public class AccountService
    {
        CBNDatabse database = new CBNDatabse();
        public event WhistleBlowerEventHandler WhistleBlowing; //fire alarm event handler
        public EventHandler <UserEventArgs> SendMail;
        public string Login()
        {
            StringBuilder build = new StringBuilder();
            Console.ForegroundColor = ConsoleColor.DarkBlue;
            Console.WriteLine("\tWelcome to Central Bank of Nigerial Portal");
            Console.ResetColor();
            Console.WriteLine("Enter Fullname:"); 
            var userFullname = InputValidation(Console.ReadLine(), "string");

            while (userFullname == string.Empty)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error: Enter non alphanumeric string");
                Console.ResetColor();
                Console.WriteLine("Enter Fullname:");
                userFullname = InputValidation(Console.ReadLine(), "string");
            }

            Console.WriteLine("Enter Password:");
            var userPassword = Console.ReadLine();
            Regex regex = new Regex(@"\w+");
            while (!regex.IsMatch(userPassword))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error: Password field can not be empty");
                Console.ResetColor();
                Console.WriteLine("Enter Password:");
                userPassword = InputValidation(Console.ReadLine(), "string");
            }

            var users = database.GetUsers();
            var ExistingUser = users.Where(person => userFullname.ToLower() == person.Fullname.ToLower() && 
                                        userPassword ==person.Password).ToList<UserAccount>();

            if (ExistingUser.Count == 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                build.Append("User Not found!");
                return build.ToString();
            }
            else
            {
                foreach (var item in ExistingUser)
                {
                    bool flagged = item.Fullname.ToLower().Contains("bill") || item.Fullname.ToLower().Contains("jane") || item.Fullname.ToLower().Contains("james");
                    if (flagged)
                    {
                        UserAccount flaggedUser = new UserAccount()
                        {
                            Fullname = item.Fullname,
                            Account_number = item.Account_number,
                            Bank_Name = item.Bank_Name,
                            Password = item.Password,
                            BVN_Num = item.BVN_Num
                        };
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("You're bared from performing any financial transactions.");
                        Console.ResetColor();
                         
                        OnWhistleBlower();
                        OnSendMail(flaggedUser);
                       
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.DarkGreen;
                        build.Append($"Welcome {item.Fullname} your BVN: {item.BVN_Num} is linked to your {item.Bank_Name} accountNo: {item.Account_number}");
                      
                        return build.ToString();
                    }
                   
                }
            }
            
            return build.Append("Alert the Police!").ToString();
        }
        public UserAccount Register() //BVN enrollment
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("\tWelcome to CBN BVN Registration Portal!\n\tEnsure to fill your details correctly.");
            Console.ResetColor();
            Console.WriteLine("Enter Fullname:");
            var fullname = InputValidation(Console.ReadLine(), "string");
            while (fullname == string.Empty)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error: Enter non alphanumeric string");
                Console.ResetColor();
                Console.WriteLine("Enter Fullname:");
                fullname = InputValidation(Console.ReadLine(), "string");
            }

            Console.WriteLine("Enter Account Number:");
            var account_number = InputValidation(Console.ReadLine(), "Digit");
            while (account_number == string.Empty)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error: Enter your 10 Digits Account Number");
                Console.ResetColor();
                Console.WriteLine("Enter Account Number:");
                account_number = InputValidation(Console.ReadLine(), "Digits");
            }

            Console.WriteLine("Enter Password:");
            var password = Console.ReadLine();
            while (password == string.Empty || password.Length < 4)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error: Password Field requires atleast 4 characters and can not be empty.");
                Console.ResetColor();
                Console.WriteLine("Enter Password:");
                password = Console.ReadLine();
            }

            Console.WriteLine("Enter your Bank name:");
            var bank = InputValidation(Console.ReadLine(), "string");
            while (bank == string.Empty)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error: Bank name is Invalid.");
                Console.ResetColor();
                Console.WriteLine("Enter your Bank name:");
                bank = InputValidation(Console.ReadLine(), "string");
            }

            var bvn = GenerateBVN();

            return new UserAccount { Fullname = fullname, Account_number = account_number, Password = password, BVN_Num = bvn, Bank_Name = bank };
        }
        public string CheckBVN(string accountNo)
        {
            var users = database.GetUsers();
            var userBVN = users.Where(bvn => accountNo == bvn.Account_number).ToList<UserAccount>();
            Console.WriteLine(userBVN.Count);
            foreach (var item in userBVN)
            {
                return $"your BVN is:{item.BVN_Num}";
            }
            return "BVN not found!";
        }
        private string InputValidation(string input, string returnType) //Validation Method for user input 
        {
            var digitInput = returnType.ToLower() == "digit" || returnType.ToLower() == "digits";
            foreach (char item in input)
            {
                if (returnType.ToLower() == "string" && char.IsDigit(item))
                {
                    return string.Empty;
                } else if (digitInput && !char.IsDigit(item))
                {
                    return string.Empty;
                }
            }
            return input;
        }
        private string GenerateBVN() //Method to automatically generate random number of 9 digits BVN
        {
            Random rand = new Random();
            var BVN = rand.Next(100000000, 999999999);

            return BVN.ToString();
        }

        protected virtual void OnWhistleBlower()
        {
            if(WhistleBlowing != null)
            {
                WhistleBlowing(this, EventArgs.Empty);
            }
        }
        protected virtual void OnSendMail(UserAccount flaggedUser)
        {
            if (SendMail != null)
            {
                UserEventArgs s = new UserEventArgs();
                s._user = flaggedUser;
                SendMail(this, new UserEventArgs() { _user = flaggedUser });
            }
        }
    }
}
