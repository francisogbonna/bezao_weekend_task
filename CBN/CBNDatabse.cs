﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeekendTask_16_07_2021.CBN
{
    public class CBNDatabse
    {
        public IEnumerable <UserAccount> GetUsers()
        {
            return new List<UserAccount>()
            {
                new UserAccount{ 
                    Account_number="0074387409",
                    Fullname="Pope Francis",BVN_Num="0020456",
                    Password="pope",Bank_Name="Access Bank"
                },
                new UserAccount{ 
                    Account_number="0456830022",
                    Fullname="Chinese Chikky",BVN_Num="48763009",
                    Password="chikky123",Bank_Name="Access Bank"
                },
                new UserAccount{
                    Account_number="0334578391",
                    Fullname="James Sammy",BVN_Num="55683482"
                    ,Password="password",Bank_Name="Diamond Bank"
                },
                new UserAccount{ 
                    Account_number="0903475835",
                    Fullname="Babatunde Sir-Shola",BVN_Num="null",
                    Password="pope!!!",Bank_Name="UBA Bank"
                },
                new UserAccount{ 
                    Account_number="0334987093",
                    Fullname="Jane Dara",BVN_Num="00203459",
                    Password="12345",Bank_Name="Zenith Bank"
                },
                new UserAccount{ 
                    Account_number="0074536812",
                    Fullname="Bill Francis",BVN_Num="99476234",
                    Password="12345",Bank_Name="Fidelity Bank"
                },
            };
        }
    }
}
