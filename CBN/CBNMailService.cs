﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace WeekendTask_16_07_2021.CBN
{
    public class CBNMailService
    {
        public void OnSendMail(object sender, UserEventArgs e)
        {
            Thread.Sleep(1000);
            Console.WriteLine($"Sending email to Director of Intelligence...\n");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine($"{e._user.Fullname} was spotted trying to perform financial transaction at {e._user.Bank_Name}.");
        }
    }
}
