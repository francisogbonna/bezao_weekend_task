﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace WeekendTask_16_07_2021.CBN
{
    public class CBNProgram
    {
        AccountService accountService = new AccountService();
        CBNTracker tracker = new CBNTracker();
        CBNMailService mailService = new CBNMailService();

        Regex regexDigit = new Regex(@"\d");
        Regex regexLetters = new Regex(@"[A-Za-z]");
        public void run()
        {
            
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine($"\tWelcome to CBN official portal.");
            Console.ResetColor();
            StringBuilder menu = new StringBuilder();
            menu.AppendLine("Choose opertaion:");
            menu.AppendLine("1. Check BVN");
            menu.AppendLine("2. Enroll for BVN");
            menu.AppendLine("3. Quit Application");
            Console.WriteLine(menu);
            string option = Console.ReadLine();

            while (!regexDigit.IsMatch(option))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Invalid option selected.");
                Console.ResetColor();
                Console.WriteLine(menu);
                option = Console.ReadLine();
            }

            switch (option)
            {
                case "1":
                    var loginDetails = accountService.Login();
                    Console.WriteLine(loginDetails);
                    Console.ResetColor();
                    break;
                case "2":
                    var newUser = accountService.Register();
                    Console.WriteLine($"{newUser.Fullname} your BVN: {newUser.BVN_Num} is now linked {newUser.Bank_Name} accountNo: {newUser.Account_number}");
                    StringBuilder build = new StringBuilder();
                    build.AppendLine($"Do you wish to login and verify your BVN?");
                    build.AppendLine("1. yes");
                    build.AppendLine("2. No");
                    Console.WriteLine(build);
                    var choice = Console.ReadLine();

                    while (!regexDigit.IsMatch(choice))
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Invalid option selected.");
                        Console.ResetColor();
                        Console.WriteLine(build);
                        choice = Console.ReadLine();
                    }
                    if(choice == "1")
                    {
                        var userDetails = newUserLogin(newUser);
                        Console.WriteLine(userDetails);
                    }
                    break;
                case "3":
                    Console.WriteLine("Shutting down...");
                    Thread.Sleep(1000);
                        break;
                default:
                    break;
            }

            accountService.WhistleBlowing += tracker.OnWhistleBlowing;
            accountService.SendMail += mailService.OnSendMail;

            
            var BvN = accountService.CheckBVN("0074387409");
            Console.WriteLine(BvN);
           
        }
        private static string newUserLogin(UserAccount newUser)
        {
            Regex regexLetters = new Regex(@"[A-Za-z]");
            StringBuilder stringBuild = new StringBuilder();
            Console.WriteLine("Enter Account Name:");
            var ac_name = Console.ReadLine();
            while (!regexLetters.IsMatch(ac_name))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid input.");
                Console.ResetColor();
                Console.WriteLine("Enter Account Name:");
                ac_name = Console.ReadLine();
            }
            Console.WriteLine("Enter password");
            var pass = Console.ReadLine();
            while (pass.Equals(string.Empty))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Password field can not be empty.");
                Console.ResetColor();
                Console.WriteLine("Enter Password:");
                pass = Console.ReadLine();
            }
            Console.WriteLine("Enter Account Number:");
            var acc_No = Console.ReadLine();
            Regex regexDigit = new Regex(@"\d");
            while (!regexDigit.IsMatch(acc_No))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid Account Number.");
                Console.ResetColor();
                Console.WriteLine("Enter Account Number:");
                acc_No = Console.ReadLine();
            }
            if(pass != newUser.Password || acc_No != newUser.Account_number)
            {
                stringBuild.AppendLine("Invalid Account Number ad Password.");
            }
            else
            {
                stringBuild.AppendLine($"Your BVN is: {newUser.BVN_Num} linked to your {newUser.Bank_Name} account. ");
            }
            return stringBuild.ToString();

        }
    }
}
