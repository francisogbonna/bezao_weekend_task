﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeekendTask_16_07_2021.CBN
{
    public class CBNTracker
    {
        public void OnWhistleBlowing(object sender, EventArgs e)
        {
            for (int i = 0; i < 10; i++)
            {
                Console.Beep();
            }
            Console.WriteLine("Criminal alert received: Calling the Police...");
        }
    }
}
