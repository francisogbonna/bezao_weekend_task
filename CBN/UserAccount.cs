﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeekendTask_16_07_2021.CBN
{
    public class UserAccount
    {
        public string Account_number { get; set; }
        public string Fullname { get; set; }
        public string BVN_Num { get; set; }
        public string Password { get; set; }
        public string Bank_Name { get; set; }
    }
}
