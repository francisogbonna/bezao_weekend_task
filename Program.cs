﻿using System;
using WeekendTask_16_07_2021.WordShuffle;
using System.Collections.Generic;
using System.Linq;
using WeekendTask_16_07_2021.CBN;
using WeekendTask_16_07_2021.SpecialCharacter;
using System.Threading;

namespace WeekendTask_16_07_2021
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.DarkBlue;
            Console.WriteLine("\tWelcome to my weekend task solutions");
            Console.ResetColor();
            Console.WriteLine("select options below to view solutions\n1. Word Shuffling task\n2. Special Character Shuffling\n3. BVN verification task");
            var option = Console.ReadLine();
            switch (option)
            {
                case "1":
                    Shuffle shuffles = new Shuffle();
                    Console.WriteLine("This solution shuffles a class list of interns using various methods");
                    Thread.Sleep(2000);
                    shuffles.QueryShuffling();
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.WriteLine("\n\tThe second method ");
                    Console.ResetColor();
                    Thread.Sleep(2000);
                    shuffles.MethodShuffling();
                    break;
                case "2":
                    CharShufle specialCharacter = new CharShufle();
                    Console.WriteLine("This solution generates the numerical values of special character strings.");
                    Console.WriteLine("Enter special characters and symbols");
                    string symbols = Console.ReadLine();
                    Console.WriteLine("Using sortedlist generic data type");
                    Thread.Sleep(1000);
                    var CharResult = specialCharacter.GetCharacters(symbols);
                    Console.WriteLine(CharResult);
                    Console.WriteLine("\nUsing a class object");
                    Thread.Sleep(2000);
                    var result = specialCharacter.GetCharacter(symbols);
                    Console.WriteLine(result);
                   
                    break;
                case "3":
                    CBNProgram cbn = new CBNProgram();
                    cbn.run();
                    break;
                default:
                    break;
            }
            


        }
    }
}
