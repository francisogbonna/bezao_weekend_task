﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace WeekendTask_16_07_2021.SpecialCharacter
{

    class CharShufle
    {
        private static SpecialCharDB charSetDB = new SpecialCharDB();
        private static SortedList<int, char> specialCharset = new SortedList<int, char>()
        {
            {1, ')'},
            {2, '('},
            {3, '*'},
            {4, '&'},
            {5, '^'},
            {6, '%'},
            {7, '$'},
            {8, '#'},
            {9, '@'},
            {0, '!'}
        };

        public string GetCharacters(string charsets)
        {
        StringBuilder stringBuilder = new StringBuilder();
            
            foreach (var item in charsets)
            {
                var result = from items in specialCharset where items.Value.Equals(item) select items;
                foreach (var i in result)
                {
                    stringBuilder.Append(i.Key);
                }
            }
            
            return ($"The numerical values for {charsets} is: {stringBuilder.ToString()}");
        }

        public string GetCharacter(string charsets)
        {
            StringBuilder stringBuilder = new StringBuilder();
            var charset = charSetDB.GetCharList();
            foreach (var item in charsets)
            {
                var result = from items in charset where items.Equals(item) select items;
                foreach (var i in result)
                {
                    stringBuilder.Append(i.Charkey);
                    Console.WriteLine(i.Charkey);
                }
            }
            return ($"The numerical values for {charsets} is: {stringBuilder.ToString()}");
        }

    }
}
