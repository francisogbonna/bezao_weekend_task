﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace WeekendTask_16_07_2021.SpecialCharacter
{
    public class SpecialCharDB
    {
        public IEnumerable<CharList> GetCharList()
        {
            return new List<CharList>()
            {
                new CharList{Charkey=1, CharSet=')'},
                new CharList{Charkey=2, CharSet='('},
                new CharList{Charkey=3, CharSet='*'},
                new CharList{Charkey=4, CharSet='&'},
                new CharList{Charkey=5, CharSet='^'},
                new CharList{Charkey=6, CharSet='%'},
                new CharList{Charkey=7, CharSet='$'},
                new CharList{Charkey=8, CharSet='#'},
                new CharList{Charkey=9, CharSet='@'},
                new CharList{Charkey=0, CharSet='!'},
            };
        }
    }
}
