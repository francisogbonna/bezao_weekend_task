﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeekendTask_16_07_2021.WordShuffle
{
    public class Database
    {
        public IEnumerable<Bezao> GetInterns()
        {
            return new List<Bezao>(){
                new Bezao {Id=1,Name="Fresh Francis"},
                new Bezao {Id=2,Name="chinese Chikky"},
                new Bezao {Id=3,Name="Small Sammy"},
                new Bezao {Id=4,Name="Angel Uria"},
                new Bezao {Id=5,Name="Daring Dara"},
                new Bezao {Id=6,Name= "Simple C#"},
                new Bezao {Id=7,Name="Crazy Clone"},
                new Bezao {Id=8,Name="Sunny Sparco"},
                new Bezao {Id=9,Name="Playful Pope"},
                new Bezao {Id=10,Name="Friendly Francis"},
            };
        }
    }
}
