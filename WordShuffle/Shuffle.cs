﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace WeekendTask_16_07_2021.WordShuffle
{
    class Shuffle
    {
        private static Database db = new Database();
        public void QueryShuffling()
        {
            var words = db.GetInterns();
            Random rand = new Random();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\nQuery syntax method of shuffling.......\n");
            var result = from persons in db.GetInterns()
                         orderby rand.Next()
                         select persons.Name;
            Console.ResetColor();
            foreach (var name in result)
            {
                Console.WriteLine(name);
            }
        }
        public void MethodShuffling()
        {
            var words = db.GetInterns();
            Random rand = new Random();
            var results = words.OrderBy(person => rand.Next());

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"\nLinq syntax method for shuffling..........\n");
            Console.ResetColor();
            foreach (var person in results)
            {
                Console.WriteLine(person.Name);
            }
        }
    }
}
