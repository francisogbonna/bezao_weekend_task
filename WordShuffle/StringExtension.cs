﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeekendTask_16_07_2021.WordShuffle
{
    public static class StringExtension
    {
        //Extension Method for shuffling words 
        public static string ShuffleWord(this string word)
        {
            char[] letter = word.ToCharArray();
            Random randomNumber = new Random();
            int itemChecker = letter.Length;

            while (itemChecker > 1)
            {
                itemChecker--;
                int temp = randomNumber.Next(itemChecker + 1);
                var value = letter[temp];
                letter[temp] = letter[itemChecker];
                letter[itemChecker] = value;
            }
            return new string(letter);
        }
    }
}
